

    ┏━┓╻ ╻┏━┓╺┳╸   ┏━╸╻╻  ┏━╸   ┏━╸┏┳┓┏━┓
    ┣━┛┣━┫┣━┫ ┃    ┣╸ ┃┃  ┣╸    ┃  ┃┃┃┗━┓
    ╹  ╹ ╹╹ ╹ ╹    ╹  ╹┗━╸┗━╸   ┗━╸╹ ╹┗━┛



    No one can construct for you the bridge
    upon which precisely you must cross the
    stream of life, no one but you yourself
    alone. [Nietzsche]


[phatfilecms](https://codeberg.org/konsumi/phatfilecms) Empowers your distributed content

[![build](https://ci.codeberg.org/api/badges/konsumi/phatfilecms/status.svg)](https://ci.codeberg.org/konsumi/phatfilecms)
