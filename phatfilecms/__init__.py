"""phatfilecms"""

import os

from flask import Flask, send_from_directory, jsonify
from flask_admin import Admin
from flask_admin.contrib.fileadmin import FileAdmin
from werkzeug.security import safe_join

from .cmd import cmd_mkjson, cmd_ls
from .phatfs import tree, get_fs


def create_app(config=None) -> Flask:
    """pathfilecms factory"""
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
    )

    if config is not None:
        app.config.from_mapping(config)
    else:
        app.config.from_pyfile(os.getenv("PHATFILECMS_SETTINGS", "settings.py"))

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.cli.add_command(cmd_mkjson)
    app.cli.add_command(cmd_ls)

    path = app.config["DATA_DIR"]
    print(get_fs(path).entities())

    admin = Admin(app, name='phatfilecms')

    admin.add_view(FileAdmin(path, '/pfs/', name='Phatfiles'))

    @app.route("/")
    def index():
        return "<p>Hello, phatfilecms <a href=/admin>admin</a>!</p>"

    @app.route("/data/<string:name>.json")
    def data(name):
        return jsonify(tree(safe_join(app.config["DATA_DIR"], name)))

    @app.route("/stream/<string:name>.json")
    def stream(name):
        return send_from_directory(
            app.config["STREAM_DIR"], name + ".json", mimetype="application/json"
        )

    return app
