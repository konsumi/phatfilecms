"""phatfilecms command package"""

import sys
import click
from flask import current_app as app
from flask.cli import with_appcontext
from werkzeug.security import safe_join

from .phatfs import tree, ls


@click.command(name="mkjson")
@click.argument("stream")
@with_appcontext
def cmd_mkjson(stream):
    import json

    print(
        json.dumps(
            tree(safe_join(app.config["DATA_DIR"], stream)),
            indent=2,
            sort_keys=True,
            default=str,
        )
    )


@click.command(name="ls")
@click.argument("stream")
@click.argument("pattern", type=str, default=None, required=False)
@with_appcontext
def cmd_ls(stream, pattern):
    try:
        for path in ls(safe_join(app.config["DATA_DIR"], stream), pattern):
            print(path)
    except FileNotFoundError as e:
        print(e)
        sys.exit(1)
