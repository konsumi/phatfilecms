"""phatfs"""

import os
import errno
from datetime import datetime
from cerberus import Validator
import yaml


class PhatEntity:
    def __init__(self, path: str, schema):
        self.path = path
        self.schema = schema

    def __repr__(self):
        return self.path


class Phatfs:
    def __init__(self, root: str):
        self.root = root

    def entities(self) -> set[PhatEntity]:
        return {PhatEntity(elem.split('/')[2], elem) for elem in
                _find_schema(self.root)}


def get_fs(root: str) -> Phatfs:
    return Phatfs(root)


def _find_schema(path: str):
    for i in os.scandir(path):
        if i.name == "schema.yaml":
            yield i.path
        elif i.is_dir(follow_symlinks=False):
            for p in _find_schema(i.path):
                yield p


def add_id(d, uid):
    if isinstance(d, dict):
        d["id"] = uid
    return d


def get_content(path):
    with open(path, mode="r", encoding="utf-8") as file:
        return file.read().rstrip("\n")


def validate(v, data):
    if v:
        if v.validate(data):
            return v.document
        else:
            raise Exception(v.errors)
    else:
        return data


class MyNormalizer(Validator):
    def __init__(self, *args, **kwargs):
        super(MyNormalizer, self).__init__(*args, **kwargs)

    def _normalize_coerce_date(self, value):
        return datetime.strptime(value, "%Y-%m-%d").date()

    def _normalize_coerce_datetime(self, value):
        return datetime.strptime(value, "%Y-%m-%d %H:%M:%S.%f")


def tree(path):
    hierarchy = {}
    asList = False
    has_files = False
    overrides = []
    val = None

    try:
        it = os.scandir(path)
        for item in it:
            if item.name == ".skip":
                continue
            if item.name == ".list":
                asList = True
                continue
            if item.name == "schema.yaml":
                val = MyNormalizer(
                    yaml.load(
                        get_content(path + os.path.sep + "schema.yaml"),
                        Loader=yaml.SafeLoader,
                    )
                )
                val.require_all = True
                continue

            if os.path.isfile(item.path):
                if item.name.find(".", 1) > 1:
                    overrides.append(item.name)
                    continue

                has_files = True
                hierarchy[item.name] = get_content(item.path)

            else:
                if os.path.exists(item.path + "/.skip"):
                    continue
                hierarchy[item.name] = tree(item.path)

    except OSError as e:
        if e.errno != errno.ENOTDIR:
            raise

    if len(overrides):
        for n in overrides:
            ns = n.split(".", 1)
            hierarchy[ns[0]][ns[1]] = get_content(path + "/" + n)

    if asList:
        return list(hierarchy.values())

    if not has_files:
        return [validate(val, add_id(v, k)) for k, v in hierarchy.items()]

    return hierarchy


def ls(path, pattern=None):
    for item in os.scandir(path):
        if item.name == ".skip":
            continue
        if item.name == ".list":
            continue
        if item.name == "schema.yaml":
            continue

        if os.path.isfile(item.path):
            if item.name.find(".", 1) > 1:
                continue
            if not pattern or pattern in item.path:
                yield item.path

        else:
            if os.path.exists(item.path + "/.skip"):
                continue
            for path in ls(item.path, pattern):
                yield path
