"""phatfilecms - cms for distributed content"""

from setuptools import setup, find_packages

setup(
    name='phatfilecms',
    author='coco von miconoco',
    author_email='coco@miconoco.de',
    version='0.1',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Cerberus==1.3.4',
        'PyYAML==5.4.1',
        'Flask==2.2.2',
        'Flask-Admin==1.6.1',
        'Werkzeug==2.3.7',
        'Jinja2==3.0.0',
        'WTForms==3.1.1',
    ],
)
