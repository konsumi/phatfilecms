# Research

Similar ideas, production ready: [Tina is a fully open-source headless CMS that supports Git](https://tina.io/)

- [awesome-python-models](https://github.com/grundic/awesome-python-models "A curated list of awesome Python libraries, which implement models, schemas, serializers/deserializers, ODM's/ORM's, Active Records or similar patterns.")

## ORM

- [datafiles](https://github.com/jacebrowning/datafiles "A file-based ORM for Python dataclasses.") - MIT
- [datastore.objects](https://github.com/datastore/datastore.objects) - simple object mapper on top of [datastore](https://github.com/jbenet/datastore)
- [ascetic](https://bitbucket.org/emacsway/ascetic/src/master/) - lightweight datamapper ORM - MIT
- [peewee](https://github.com/coleifer/peewee "small, expressive orm -- supports postgresql, mysql and sqlite" ) - MIT
    - [adding a new database driver](http://docs.peewee-orm.com/en/latest/peewee/database.html#adding-a-new-database-driver)

## Storage / Model

- https://github.com/Panaetius/git_storage_test
- https://github.com/drotiro/dbtoy
- [extend tinydb](https://tinydb.readthedocs.io/en/latest/extend.html)
    - https://github.com/alshapton/TinyMP
    - https://github.com/patarapolw/tinydb-constraint
    - https://github.com/schapman1974/tinymongo
- [simple model framework](https://github.com/afriemann/simple_model) - BSD-3

## Schema

- [Yamale](https://github.com/23andMe/Yamale) - MIT
- [Scheme](https://github.com/jordanm/scheme "A declarative schema framework for Python.") - BSD-3
- [Schema](https://github.com/marrow/schema/ "A generic declarative schema system.") - MIT
- [Schematics](https://github.com/schematics/schematics "Python Data Structures for Humans™.") - BSD-3
- [Dynamic schemas in marshmallow](https://stevenloria.com/dynamic-schemas-in-marshmallow/)
- [Dynamic schemas in SQLAlchemy](http://sparrigan.github.io/sql/sqla/2016/01/03/dynamic-tables.html)
- [pyvaru](https://github.com/daveoncode/pyvaru "Rule based data validation library for python 3.") - MIT
- [valideer](https://github.com/podio/valideer "Lightweight data validation and adaptation Python library.") - MIT


## file based wiki

- [quiki](https://github.com/cooper/quiki) - [ISC](https://github.com/cooper/quiki/blob/master/LICENSE) - golang
- [guetzli](https://github.com/muellermichel/guetzli) - LGPL3.0



## Flask-Admin Themes

- https://www.creative-tim.com/templates/flask
- https://codedthemes.com/item/category/templates/flask-template/


## Python

- [what are metaclasses](https://stackoverflow.com/questions/100003/what-are-metaclasses-in-python)
