
ifeq ($(OS),Windows_NT)
EBIN      ?= .venv/Scripts
PY3       := python
else
EBIN      ?= .venv/bin
PY3       := python3
endif
PY        := $(EBIN)/$(PY3)
SETTINGS  ?= $(PWD)/settings.py

FLASK     := PHATFILECMS_SETTINGS="$(SETTINGS)" \
		FLASK_RUN_EXTRA_FILES="$(SETTINGS)" \
		FLASK_ENV=development \
		FLASK_APP=phatfilecms \
		PYTHONDONTWRITEBYTECODE=1 \
		$(PY) -m flask

include config.mk

JSTREAMS   := $(patsubst %,$(DST)/%,$(STREAMS:=.json))

help: Makefile
	@sed -n 's/^##//p' $<

## all          generates all streams
all: install $(JSTREAMS)

## build        creates our distribution package
build: install
ifdef DEV
	$(PY) setup.py sdist
else
	$(PY) setup.py release sdist
endif

## run          serves our webend
run: all
	@$(FLASK) run

## archives     generates archives from tree
archives: $(DST)/tree.tgz $(DST)/tree.sqfs

$(DST)/%.json: $(SRC)/%
	@echo mk $@
	@mkdir -p $(@D)
	@$(FLASK) mkjson $* > $@

$(DST)/tree.json: $(SRC)
	@echo mk $@
	@mkdir -p $(@D)
	@$(FLASK) mkjson . > $@

$(DST)/tree.tgz: $(SRC)
	tar -cvzf $@ -C $< .

$(DST)/tree.sqfs: $(SRC)
	mksquashfs $< $@ -comp zstd
	@echo "mkdir /tmp/phatfiles && mount -t squashfs -o loop ./$@ /tmp/phatfiles"

## install      installs dependencies
install: .venv

.venv: requirements.txt setup.py
	$(PY3) -m venv $@
	@echo Signature: 8a477f597d28d172789f06886806bc55 > $@/CACHEDIR.TAG
	$(PY) -m pip install --upgrade pip
	$(PY) -m pip install -r $<
	@touch $@

## clean        removes all generated files
clean:
	@rm -f -- $(JSTREAMS) $(DST)/tree.tgz $(DST)/tree.sqfs

## distclean    purges all generated files
distclean: clean
	@rm -rf -- dist *.egg-info __pycache__ .venv

.PHONY: all archives build clean help install run
